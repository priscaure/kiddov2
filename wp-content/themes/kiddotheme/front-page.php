<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::context();
$context['posts'] = Timber::get_posts(); //leave blank for default page query
$context['home'] = get_field('home');
$context['home_header'] = get_field('home_header');
$context['kiddoz_kiddo'] = get_field('kiddoz_by_kiddo');
$context['kiddoz_is'] = get_field('kiddoz_est');
$context['labels'] = get_field('labels');
$context['instagram_widget'] = Timber::get_widgets( 'instagram_widget' );
$context['facebook_widget'] = Timber::get_widgets( 'facebook_widget' );
$context['twitter_widget'] = Timber::get_widgets( 'twitter_widget' );
$context['kiddo_life'] = get_field('kiddo_life');
$context['homeslider'] = get_field('homeslider');
$context['slider_video'] = get_field('slider_video');
$context['latest_news'] = get_field('latest_news');
if (is_front_page()){
    // get latest post
    $args = array(
        'posts_per_page' => 1
    );
    $context['latest_post'] = Timber::get_posts($args);
}
Timber::render('front-page.twig', $context);