<?php
/*
Template Name: Cart
*/

$context = Timber::get_context();
$context['post'] = new TimberPost();
$context['titre_kiddo'] = get_field('titre_kiddo');
$template = ['page-my-account.twig'];
Timber::render($template, $context);