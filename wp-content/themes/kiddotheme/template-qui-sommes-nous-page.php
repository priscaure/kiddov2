<?php
/**
 * Template Name: A propos
 * Description: Une page qui raconte l'histoire de kiddo
 */

$context = Timber::context();

$timber_post = new Timber\Post();
$context['post'] = $timber_post;
$context['about'] = get_field('a_propos');
Timber::render( array( 'page-' . $timber_post->post_name . '.twig', 'page.twig' ), $context );