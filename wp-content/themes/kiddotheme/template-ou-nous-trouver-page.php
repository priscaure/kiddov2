<?php
/**
 * Template Name: Nos points de vente
 * Description: Une page qui display les points de vente
 */

$context = Timber::context();

$partners_query = array(
    'post_type' => 'partenaires',
    'posts_per_page' => -1,
    'post_status' => 'publish'
);

$timber_post = new Timber\Post();
$context['post'] = $timber_post;
$context['partners'] = Timber::get_posts($partners_query);
$context['vente'] = get_field('points_de_vente');
$context['partenaires'] = get_field('nos_partenaires');
Timber::render( array( 'page-' . $timber_post->post_name . '.twig', 'page.twig' ), $context );