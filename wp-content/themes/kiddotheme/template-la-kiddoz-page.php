<?php
/**
 * Template Name: La Kiddoz
 * Description: Une page qui explique le fonctionnement de la kiddo'z
 */

$context = Timber::context();

$timber_post = new Timber\Post();
$context['post'] = $timber_post;
$context['la_kiddoz'] = get_field('la_kiddoz');
Timber::render( array( 'page-' . $timber_post->post_name . '.twig', 'page.twig' ), $context );