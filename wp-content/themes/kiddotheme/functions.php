<?php
/**
 * Timber starter-theme
 * https://github.com/timber/starter-theme
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	});

	add_filter('template_include', function( $template ) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});

	return;
}

/**
 * Sets the directories (inside your theme) to find .twig files
 */
Timber::$dirname = array( 'templates', 'views' );

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;


/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class StarterSite extends Timber\Site {
	/** Add timber support. */
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
		add_filter( 'timber/context', array( $this, 'add_to_context' ) );
		add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		parent::__construct();
	}
	/** This is where you can register custom post types. */
	public function register_post_types() {

	}
	/** This is where you can register custom taxonomies. */
	public function register_taxonomies() {

	}

	/** This is where you add some context
	 *
	 * @param string $context context['this'] Being the Twig's {{ this }}.
	 */
	public function add_to_context( $context ) {
		$context['foo'] = 'bar';
		$context['stuff'] = 'I am a value set in your functions.php file';
		$context['notes'] = 'These values are available everytime you call Timber::context();';
		$context['menu'] = new Timber\Menu('Top Menu');
		$context['site'] = $this;
		$context['titre_kiddo'] = get_field('titre_kiddo');
		$context['options'] = get_fields('option');
		$context['compatibilite'] = get_field('compatibilite');
		$context['gmaps'] = get_field('google_maps');
		$context['mailchimp_widget'] = Timber::get_widgets( 'mailchimp_widget' );
		$context['footer_sidebar_1'] = Timber::get_widgets( 'footer_sidebar_1' );
		$context['footer_sidebar_2'] = Timber::get_widgets( 'footer_sidebar_2' );
		$context['footer_sidebar_3'] = Timber::get_widgets( 'footer_sidebar_3' );
		$context['socials'] = get_field('socials', 'options');
		return $context;
	}

	public function theme_supports() {
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5', array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support(
			'post-formats', array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'audio',
			)
		);

		add_theme_support( 'menus' );
	}

	/** This Would return 'foo bar!'.
	 *
	 * @param string $text being 'foo', then returned 'foo bar!'.
	 */
	public function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	/** This is where you can add your own functions to twig.
	 *
	 * @param string $twig get extension.
	 */
	public function add_to_twig( $twig ) {
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter( new Twig_SimpleFilter( 'myfoo', array( $this, 'myfoo' ) ) );
		return $twig;
	}

}

new StarterSite();

function kiddotheme_enqueue_scripts() {
	//wp_enqueue_script( 'jquery-cdn', '//code.jquery.com/jquery-3.3.1.min.js', array(), '3.3.1', true );
	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js', array('jquery'), '', true );
	wp_enqueue_script( 'popper-js', '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'bootstrap-js', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'google-map', '//maps.googleapis.com/maps/api/js?key=AIzaSyD35fElB7kKbeHQLxyu3u6cx_MoVVRtHzk', array(), '3', true );
	wp_enqueue_script( 'google', get_template_directory_uri() . '/js/google.js', array('google-map', 'jquery'), '0.1', true );
	wp_enqueue_script( 'nprogress-js', get_template_directory_uri() . '/js/nprogress.js', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'kiddotheme_enqueue_scripts' );

function getRelatedPosts($post) {
    $categories_ids = [];

    foreach ($post->terms as $term) {
        array_push($categories_ids, $term->id);
    }

    $related_posts = Timber::get_posts(array(
        'posts_per_page' => 3,
        'category__in'  => $categories_ids,
        'post__not_in' => array($post->ID),
    ));

    return $related_posts;
}

/**
 * ACF Add Options Page 
 *
 */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> true,
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
}

/**
 * Register our sidebars and widgetized areas.
 *
 */
function kiddo_widgets_init() {
	
	register_sidebar( array(
		'name' => 'Instagram Widget',
		'id' => 'instagram_widget',
		'description' => 'Appears in the kiddo life area',
		'before_widget' => '<div class="row insta-gallery">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="widget-title insta-gallery__title">',
		'after_title' => '</h2>',
	) );

	register_sidebar( array(
		'name' => 'Facebook Widget',
		'id' => 'facebook_widget',
		'description' => 'Appears in the last news area',
		'before_widget' => '<div class="card-body">',
		'after_widget' => '</div>',
	) );

	register_sidebar( array(
		'name' => 'Twitter Widget',
		'id' => 'twitter_widget',
		'description' => 'Appears in the last news area',
		'before_widget' => '<div class="card-body">',
		'after_widget' => '</div>',
	) );

	register_sidebar( array(
		'name' => 'Mailchimp Widget',
		'id' => 'mailchimp_widget',
		'description' => 'Appears before the footer area',
		'before_widget' => '<form class="form-newsletter">',
		'after_widget' => '</form>',
		'before_title' => '<h6>',
		'after_title' => '</h6>',
	) );

	register_sidebar( array(
		'name' => 'Footer Sidebar 1',
		'id' => 'footer_sidebar_1',
		'description' => 'Appears in the footer area',
		'before_widget' => '<nav class="footer-menu">',
		'after_widget' => '</nav>',
		'before_title' => '<h3 class="footer-menu__title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => 'Footer Sidebar 2',
		'id' => 'footer_sidebar_2',
		'description' => 'Appears in the footer area',
		'before_widget' => '<nav class="footer-menu">',
		'after_widget' => '</nav>',
		'before_title' => '<h3 class="footer-menu__title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => 'Footer Sidebar 3',
		'id' => 'footer_sidebar_3',
		'description' => 'Appears in the footer area',
		'before_widget' => '<nav class="footer-menu">',
		'after_widget' => '</nav>',
		'before_title' => '<h3 class="footer-menu__title">',
		'after_title' => '</h3>',
	) );
}
add_action( 'widgets_init', 'kiddo_widgets_init' );

/**
 * Add Woocommerce
 *
 */

function theme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'theme_add_woocommerce_support' );

/**
 * Add Maps ACF
 *
 */
function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyD35fElB7kKbeHQLxyu3u6cx_MoVVRtHzk');
}
add_action('acf/init', 'my_acf_init');

/*
* Custom post type 'Partenaires'
*/

function kiddo_custom_post_type() {

	// On rentre les différentes dénominations de notre custom post type qui seront affichées dans l'administration
	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'Partenaires', 'Post Type General Name'),
		// Le nom au singulier
		'singular_name'       => _x( 'Partenaire', 'Post Type Singular Name'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Partenaires'),
		// Les différents libellés de l'admin
		'all_items'           => __( 'Tous les partenaires'),
		'view_item'           => __( 'Voir les partenaires'),
		'add_new_item'        => __( 'Ajouter un nouveau partenaires'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer le partenaire'),
		'update_item'         => __( 'Modifier le partenaire'),
		'search_items'        => __( 'Rechercher un partenaire'),
		'not_found'           => __( 'Non trouvé'),
		'not_found_in_trash'  => __( 'Non trouvé dans la corbeille'),
	);
	
	// On peut définir ici d'autres options pour notre custom post type
	
	$args = array(
		'label'               => __( 'Partenaires'),
		'description'         => __( 'Tous les partenaires où vous pouvez trouver la kiddo\'z'),
		'labels'              => $labels,
		// On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		/* 
		* Différentes options supplémentaires
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'has_archive'         => true,
		'supports'			  => array('title', 'thumbnail'),
		'rewrite'			  => array( 'slug' => 'partenaires'),
		'menu_position'       => 8,
		'menu_icon'           => 'dashicons-location-alt',
	);
	
	// On enregistre notre custom post type qu'on nomme ici "serietv" et ses arguments
	register_post_type( 'partenaires', $args );

}
add_action( 'init', 'kiddo_custom_post_type', 0 );

add_action( 'init', 'kiddo_add_taxonomies', 0 );

//On crée une taxonomie personnalisées: Catégories de partenaire.

function kiddo_add_taxonomies() {
	
	// Catégorie de partenaire

	$labels_cat_partner = array(
		'name'                       => _x( 'Catégories de partenaire', 'taxonomy general name'),
		'singular_name'              => _x( 'Catégories de partenaire', 'taxonomy singular name'),
		'search_items'               => __( 'Rechercher une catégorie'),
		'popular_items'              => __( 'Catégories populaires'),
		'all_items'                  => __( 'Toutes les catégories'),
		'edit_item'                  => __( 'Editer une catégorie'),
		'update_item'                => __( 'Mettre à jour une catégorie'),
		'add_new_item'               => __( 'Ajouter une nouvelle catégorie'),
		'new_item_name'              => __( 'Nom de la nouvelle catégorie'),
		'add_or_remove_items'        => __( 'Ajouter ou supprimer une catégorie'),
		'choose_from_most_used'      => __( 'Choisir parmi les catégories les plus utilisées'),
		'not_found'                  => __( 'Pas de catégories trouvées'),
		'menu_name'                  => __( 'Catégories de partenaire'),
	);

	$args_cat_partner = array(
	// Si 'hierarchical' est défini à true, notre taxonomie se comportera comme une catégorie standard
		'hierarchical'          => true,
		'labels'                => $labels_cat_partner,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'categories-partenaires' ),
	);

	register_taxonomy( 'categoriespartenaires', 'partenaires', $args_cat_partner );
}

/**
 * Woocommerce product to get the right context
 *
 */
function timber_set_product( $post ) {
    global $product;
    
    if ( is_woocommerce() ) {
        $product = wc_get_product( $post->ID );
    }
}