<?php
/**
 * Template Name: Notre Histoire
 * Description: Une page qui raconte l'histoire de kiddo
 */

$context = Timber::context();

$timber_post = new Timber\Post();
$context['post'] = $timber_post;
Timber::render( array( 'page-' . $timber_post->post_name . '.twig', 'page.twig' ), $context );