jQuery(document).ready(function($) {

    // Show the progress bar 
    NProgress.start();

    // Increase randomly
    var interval = setInterval(function() { NProgress.inc(); }, 1000);        

    // Trigger finish when page fully loaded
    jQuery(window).load(function () {
        clearInterval(interval);
        NProgress.done();
    });

    // Trigger bar when exiting the page
    window.onbeforeunload = function() {
        console.log("triggered");
        NProgress.start();
    };
        
    // NAVBAR
    $(function() {
        var header = $(".navbar");
  
        $(window).scroll(function() {    
            var scroll = $(window).scrollTop();
            if (scroll >= 60) {
                header.addClass("navbar--white");
            } else {
                header.removeClass("navbar--white");
            }
        });
    });

    $(window).scroll(function(){
        if ($(window).scrollTop() >= 83) {
            $('.alert-kiddo').addClass('fixed-alert').fadeIn();
        }
        else {
            $('.alert-kiddo').removeClass('fixed-alert').fadeOut();
        }
    });

    // Video Modal
    var $videoSrc;  
    $('.video-btn').on('click', function() {
        $videoSrc = $(this).data( "src" );
    });

    // when the modal is opened autoplay it  
     $('.modal-video').on('shown.bs.modal', function (e) {
        // set the video src to autoplay and not to show related video. 
        $(".video-item").attr('src',$videoSrc + "?modestbranding=1&amp;showinfo=0&amp;relatedToVideoId=HG34MyVaY4g" ).delay(4000).fadein(400); 
    }) 

    // stop playing the youtube video when I close the modal
    $('.modal-video').on('hide.bs.modal', function (e) {
        $(".video-item").attr('src',$videoSrc); 
    })

    $('.modal-video').on('hidden.bs.modal', function (e) {
        $(".video-item").removeAttr('src');
    })

});
