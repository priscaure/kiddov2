<?php
/**
 * The home template file
 *
 * If the user has selected a blog page to display their articles, this is what will
 * appear.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::context();
$context['posts'] = Timber::get_posts(); //leave blank for default page query
$blog = get_option('page_for_posts');
$featured = get_field('featured_post', $blog);
if($featured) {
	$featured_ID = $featured->ID;
	$featured = new TimberPost($featured);
}
$context['featured'] = $featured;
Timber::render('home.twig', $context);